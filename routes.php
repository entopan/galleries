<?php

Route::group(
    [
        'prefix' => 'api/v1/public/galleries',
        'middleware' => [
            'api'
        ],
    ], function () {
    Route::get('albums', ['uses' => 'Entopancore\Galleries\Http\Controllers\GalleriesController@albums']);
    Route::get('albums/{id}', ['uses' => 'Entopancore\Galleries\Http\Controllers\GalleriesController@album']);
    Route::get('galleries', ['uses' => 'Entopancore\Galleries\Http\Controllers\GalleriesController@galleries']);
    Route::get('galleries/{id}', ['uses' => 'Entopancore\Galleries\Http\Controllers\GalleriesController@gallery']);
    Route::get('galleries/{id}/items', ['uses' => 'Entopancore\Galleries\Http\Controllers\GalleriesController@galleryItems']);
});