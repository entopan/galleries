<?php namespace Entopancore\Galleries;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{


    public function register()
    {
        \App::register('Entopancore\Galleries\Http\GalleryServiceProvider');
    }

}
