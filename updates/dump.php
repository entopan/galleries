<?php namespace Entopancore\Galleries\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Illuminate\Database\Schema\Blueprint;

class Dump extends Migration
{
    public function up()
    {
        $file = \File::get(plugins_path() . "/entopancore/galleries/updates/versions/v1.sql");
        \DB::unprepared($file);
    }

    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('entopancore_galleries_albums');
        Schema::dropIfExists('entopancore_galleries_galleries');
        Schema::dropIfExists('entopancore_galleries_galleries_items');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}