<?php namespace Entopancore\Galleries\Http\Controllers;

use Entopancore\Galleries\Http\Repositories\GalleryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class GalleriesController extends Controller
{
    public $request;
    public $gallery;


    public function __construct(GalleryRepositoryInterface $gallery, Request $request, $params = null)
    {
        $this->request = $request;
        $this->gallery = $gallery;
    }


    public function album($albumId): Response
    {
        if ($album = $this->gallery->album($albumId)) {
            return getSuccessResult($album);
        }

        return getNotFoundResult();
    }


    public function albums(): Response
    {
        if ($albums = $this->gallery->albums($this->request->header('api-type'), $this->request->header('api-order-field'), $this->request->header('api-order-type'), $this->request->header("api-take"), $this->request->header("api-skip"), $this->request->header('api-where'))) {
            return getSuccessResult($albums);
        }
        return getNotFoundResult();
    }


    public function galleries(): Response
    {
        if ($galleries = $this->gallery->galleries($this->request->header('api-type'), $this->request->header('api-order-field'), $this->request->header('api-order-type'), $this->request->header("api-take"), $this->request->header("api-skip"), $this->request->header('api-where'))) {
            return getSuccessResult($galleries);
        }

        return getNotFoundResult();
    }


    public function gallery($galleryId): Response
    {
        $gallery = \Cache::store("api")->remember("gallery-" . $galleryId, 1800, function () use ($galleryId) {
            return $this->gallery->gallery($galleryId);
        });
        if ($gallery) {
            return getSuccessResult($gallery);
        }

        return getNoContentResult();
    }


    public function galleryItems($galleryId): Response
    {
        $gallery = \Cache::store("api")->remember("gallery-items-" . $galleryId, 1800, function () use ($galleryId) {
            return $this->gallery->galleryItems($galleryId);
        });

        if ($gallery) {
            return getSuccessResult($gallery);
        }

        return getNoContentResults();
    }


}