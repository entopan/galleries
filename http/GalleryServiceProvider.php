<?php namespace Entopancore\Galleries\Http;

use Illuminate\Support\ServiceProvider;

class GalleryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind("Entopancore\Galleries\Http\Repositories\GalleryRepositoryInterface", "Entopancore\Galleries\Http\Repositories\EloquentGalleryRepository");
    }

}
