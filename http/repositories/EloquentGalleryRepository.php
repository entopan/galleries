<?php namespace Entopancore\Galleries\Http\Repositories;

use Entopancore\Galleries\Models\Album;
use Entopancore\Galleries\Models\Gallery;
use Entopancore\Galleries\Models\GalleryItem;


class EloquentGalleryRepository implements GalleryRepositoryInterface
{

    public function galleries($type, $fieldOrder, $typeOrder, $take, $skip, $where = null): array
    {
        return Gallery::getModel($type, $fieldOrder, $typeOrder, $take, $where, ['active'], ['items']);
    }

    public function gallery($id): array
    {
        $gallery = Gallery::active()->with(["items" => function ($query) {
            $query->with("page");
        }])->find($id);
        if ($gallery) {
            return $gallery->toArray();
        } else {
            return [];
        }
    }

    public function galleryItems($galleryId): array
    {
        $galleryItems = GalleryItem::whereHas("gallery", function ($query) {
            $query->active();
        })->with("page")->where("gallery_id", "=", $galleryId)->orderBy("sort_order", "asc")->get();
        if ($galleryItems->count() > 0) {
            return $galleryItems->toArray();
        } else {
            return array();
        }
    }

    public function album($id): array
    {
        return Album::with("attachments")->find($id)->toArray();
    }

    public function albums($type, $fieldOrder, $typeOrder, $take, $skip, $where = null): array
    {
        return Album::getModel($type, $fieldOrder, $typeOrder, $take, $where, null, ['attachments']);
    }


}