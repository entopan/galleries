<?php
/**
 * Created by PhpStorm.
 * User: entopancore
 * Date: 05/06/17
 * Time: 9.23
 */

namespace Entopancore\Galleries\Http\Repositories;


interface GalleryRepositoryInterface
{

    public function galleries($type, $fieldOrder, $typeOrder, $take, $skip, $where): array;

    public function gallery($id): array;

    public function album($id): array;

    public function galleryItems($galleryId): array;

    public function albums($type, $fieldOrder, $typeOrder, $take, $skip, $where): array;


}