<?php namespace Entopancore\Galleries\Controllers;

use Backend\Classes\Controller;
use BackendMenu;


class Galleries extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
        'Backend\Behaviors\ReorderController',
        'Entopancore\Utility\Behaviors\MediaController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $mediaConfig = ["type" => "nested"];

    public $requiredPermissions = ['entopancore.galleries.superadmin', 'entopancore.galleries.admin'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Galleries', 'galleries', 'galleries');
    }

    public function reorderExtendQuery($query)
    {
        $query->where("gallery_id", "=", $this->params[0]);
    }


    public function relationExtendManageWidget($widget, $field)
    {
        switch ($field) {
            case "products":
                $widget->bindEvent('list.extendQueryBefore', function ($query) {
                    return $query->variations();
                });
                break;
        }
    }


}
