<?php namespace Entopancore\Galleries\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Albums extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Entopancore\Utility\Behaviors\MediaController',
        'Backend\Behaviors\RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $mediaConfig = ["type" => "nested"];


    public $requiredPermissions = ['entopancore.galleries.superadmin', 'entopancore.galleries.admin'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Galleries', 'galleries', 'albums');
    }


}
