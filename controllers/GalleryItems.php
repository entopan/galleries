<?php namespace Entopancore\Galleries\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class GalleryItems extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        "Entopancore\Utility\Behaviors\MediaController"
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['entopancore.galleries.superadmin', 'entopancore.galleries.admin'];
    public $mediaConfig = ["type" => "foreign", "foreign" => "gallery_id", "folder" => "gallery"];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Galleries', 'galleries', 'galleries');
    }
}