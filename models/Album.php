<?php namespace Entopancore\Galleries\Models;

use Entopancore\Utility\Traits\Folderable;
use Entopancore\Utility\Traits\Modelable;
use Model;
use Cms\Classes\Page;


/**
 * Model
 */
class Album extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Modelable;
    use Folderable;
    /*
     * Validation
     */

    public $rules = [
        'title' => 'required',
        'slug' => 'required|unique:entopancore_galleries_albums'
    ];
    public $folderable = [
        "type" => "nested", //
        "images" => [
            "image_header_default" => null,
            "image_default" => null,
            "image_grid" => null,
            "image_thumb" => null,
            "image_twitter" => null,
            "image_facebook" => null,
            "image_header" => null
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    public $attachMany = [
        'attachments' => 'System\Models\File',
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_galleries_albums';


    public function filterFields($fields, $context = null)
    {
        if ($context == 'update') {
            $fields->slug->disabled = 1;
        }
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }


}