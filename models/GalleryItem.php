<?php namespace Entopancore\Galleries\Models;

use Entopancore\Utility\Traits\Folderable;
use Entopancore\Utility\Traits\Modelable;
use Model;
use October\Rain\Database\Traits\Sortable;

/**
 * Model
 */
class GalleryItem extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sortable;
    use Modelable;
    use Folderable;

    /*
     * Validation
     */

    public $rules = [
    ];

    public $appends = ["link"];

    public $jsonable = ['parameters'];

    public $implement = ['Entopancore.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['title', 'description'];


    public $folderable = [
        "folder" => "gallery",
        "foreign" => "gallery_id",
        "images" => [
            "image_header_default" => "photo",
            "image_default" => "photo",
            "image_grid" => "photo|medium",
            "image_thumb" => "photo|mini",
            "image_twitter" => "photo|facebook",
            "image_facebook" => "photo|facebook",
            "image_header" => "photo|large"
        ],
        "type" => "foreign"
    ];

    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_galleries_galleries_items';


    public $belongsTo = [
        'page' => ['Entopancore\Urls\Models\Url', 'key' => 'page_id'],
        'gallery' => ['Entopancore\Galleries\Models\Gallery']
    ];


    public function getLinkAttribute()
    {
        return ($this->page->url) ?? ($this->page_custom) ?? null;
    }

}