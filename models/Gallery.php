<?php namespace Entopancore\Galleries\Models;

use Entopancore\Utility\Traits\Folderable;
use Entopancore\Utility\Traits\Modelable;
use Model;
use Cms\Classes\Page;

/**
 * Model
 */
class Gallery extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Modelable;

    /*
     * Validation
     */
    public $jsonable = ['parameters', 'products'];

    public $rules = [
        'title' => 'required',
        'url' => 'unique:entopancore_galleries_galleries',
        'slug' => 'required|unique:entopancore_galleries_galleries'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'entopancore_galleries_galleries';

    public $hasMany = [
        'items' => ['Entopancore\Galleries\Models\GalleryItem', 'key' => 'gallery_id'],
        'items_count' => ['Entopancore\Galleries\Models\GalleryItem', 'key' => 'gallery_id','count'=>true],

    ];


    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }


}